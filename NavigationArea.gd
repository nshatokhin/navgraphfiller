tool
extends Position2D

export (float) var width : float = 800
export (float) var height : float = 600

export (float) var vertical_spacing : float = 30
export (float) var horizontal_spacing : float = 30
export (float) var max_edge_length : float = 75

export (float) var point_radius : float = 5.0
export (Color) var point_color : Color = Color.red

export (float) var edge_width : float = 2.0
export (Color) var edge_color : Color = Color.green

var points : Dictionary = {}
var edges : Dictionary = {}

var max_edge_length_squared : float = max_edge_length * max_edge_length

func _ready():
	fill_points()
	fill_edges()
	update()

func _draw():
	for key in edges.keys():
		var edge = edges[key]
		draw_line(edge.from, edge.to, edge_color, edge_width)
	
	for key in points.keys():
		var point = points[key]
		draw_circle(point.position, point_radius, point_color)
	
func fill_points():
	var k = 0
	var shape = CircleShape2D.new()
	var radius = max(horizontal_spacing, vertical_spacing)
	var query = Physics2DShapeQueryParameters.new()
	shape.set_radius(radius)
	query.set_exclude([])
	query.set_shape(shape)
			
	for i in range(0, width, horizontal_spacing):
		for j in range(0, height, vertical_spacing):
			var pos = Vector2(i, j)
			query.set_transform(Transform2D(0.0, pos))
			var res = get_world_2d().direct_space_state.intersect_shape(query)
			
			var obstaclesIntersected = false
			if not res.empty():
				for intersection in res:
					if intersection.collider.is_in_group("Obstacles"):
						obstaclesIntersected = true
						break
						
			if not obstaclesIntersected:
				points[k] = {"position": pos}
				k += 1

func fill_edges():
	var k = 0
	var keys = points.keys()
	for i in range(0, keys.size()):
		var point1 = points[keys[i]]
		for j in range(i + 1, keys.size()):
			var point2 = points[keys[j]]
			
			if point1.position.distance_squared_to(point2.position) <= max_edge_length_squared:
				var space_state = get_world_2d().direct_space_state

				var result = space_state.intersect_ray(point1.position, point2.position)

				if result.empty() or not result.collider.is_in_group("Obstacles"):
					edges[k] = {"from": point1.position, "to": point2.position}
					k += 1
				
				


func cycle_once():
	pass
